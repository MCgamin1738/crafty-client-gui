# Crafty-Client-GUI

A simple GUI to interface with the Crafty Client Python Library

# How to use

Simply run craftygui.exe for windows or craftygui (no extension) for Linux

DO NOT USE THE .py FILE
it currently uses a modified library that isnt on pip yet

Current .py Dependencies

crafty_client
PySimpleGUI


# Planned Features

A INI Config file to set a default API Key and a better GUI (Images)
A search feature for the log button, as well as a way to save to a file

# Future OS's to add support to

MacOS, ARM (Raspberry Pi/SBC'S)